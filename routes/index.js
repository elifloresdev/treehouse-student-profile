const express = require('express'),
       router = express.Router(),
       mid = require('../middleware')


/* GET home page. */
router.get('/', function(req, res, next) {
  if(req.cookies.username) {
    res.redirect(`/student/${req.cookies.username}`)
  } else {
    res.render('index', { title: 'Index' });
  }
});

router.post('/',mid.postNotEmpty, function(req, res, next) {
    const name = (req.body.username).replace(/ /g,'')
    return res.redirect(`/student/${name}`)
});


module.exports = router;

