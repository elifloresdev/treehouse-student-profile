const express = require('express'),
       router = express.Router(),
       student = require('../middleware/treehouse'),
       mid = require('../middleware')



router.get('/', (req,res,next) => {
  if(req.cookies.username) {
    res.redirect(`/student/${req.cookies.username}`)
  } else {
    res.redirect(`/`)
  }
})

router.get('/:name', function(req, res, next) {
  const username = req.params.name
  res.cookie('username',username)
  res.locals.title = username

  function locals(body) {
    res.locals.student = body
    return res.locals.student
  }

  function render(locals) {
    return res.render('student',locals)
  }

  student.profile(username)
          .then(student.parseJson)
          .then(locals)
          .then(render)
          .catch(err => {
                  next(err)
                  res.clearCookie('username')
                })


})


router.post('/', function(req,res,next) {
  res.clearCookie('username')
  console.log('req.cookie.deleted')
  return res.redirect('/')
})
      

module.exports = router