const request = require("request")

exports.profile = (name) => {
  return new Promise((resolve,reject) => {
    request(`https://teamtreehouse.com/${name}.json`, (error,response,body) => {
        if(error) {
          console.log(error)
        } else {
          if(response.statusCode === 404) {
            const err = new Error("Couldn't Find Student")
            err.status = 404
            reject(err)
          }
          resolve(body)
        }
    })
  })
}

exports.parseJson = function(input) {
let body = ''
body = input.toString()
return JSON.parse(body)
}

