exports.postNotEmpty = (req,res,next) => {
  if (req.body.username !== '') {
    next()
  } else {
    const error = new Error('field is empty')
    error.status = 404
    next(error)
  }
}

exports.cookieRequired = (req,res,next) => {
  if(req.cookies.username) {
    next()
  } else {
    res.redirect('/')
  }
}