const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const index = require('./routes/index');
const student = require('./routes/student')

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/student/',express.static(__dirname + '/public'));

app.use(favicon(__dirname+'/public/images/favicon.ico'));

app.use('/', index);
app.use('/student', student);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {


  res.locals = {
    message: err.message,
    error: req.app.get('env') === 'development' ? err : {},
    image_url: 'http://i.imgur.com/VKKm0pn.png'
  }

  //clear cookie if student not found and change image
  if(req.cookies.username && err.message === "Couldn't Find Student") {
    res.clearCookie('username')
    res.locals.image_url = 'http://i.imgur.com/KMi72wU.png'
  }

  // render the error page
  res.status(err.status || 500);
  res.render('error', {title: 'ERROR'});
});

app.listen(3000, () => {
  console.log('Express app listening on port: 3000')
})

// module.exports = app;
